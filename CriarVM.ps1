[void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
[void][System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")

##############################################
# Estrutura, posicionamento e tamanho de tela#
##############################################
$Form1 = New-Object System.Windows.Forms.Form
$Form1.ClientSize = New-Object System.Drawing.Size(405, 490)
$Form1.Text = "Criar nova Máquina Virtual"
$Form1.topmost = $true
$Form1.StartPosition = "CenterScreen"
##############################################

############### janela inicial  ##############
##############################################
Add-Type -AssemblyName System.Windows.Forms
$Label = New-Object System.Windows.Forms.Label
$Form = New-Object system.Windows.Forms.Form
$Form.Text="Date"
$Form.AutoSize = $True
$Form.MinimizeBox = $False
$Form.MaximizeBox = $False
$Form.WindowState = "Normal"
$Form.StartPosition = "CenterScreen"

$img = [System.Drawing.Image]::Fromfile('C:\script-hyperv\icone.png')
$pictureBox = new-object Windows.Forms.PictureBox
$pictureBox.Width = $img.Size.Width
$pictureBox.Height = $img.Size.Height
$pictureBox.Image = $img
$form.controls.add($pictureBox)

[void]$Form.ShowDialog()
##############################################

############### Nome da VM ###################
##############################################
$lblNomeVM = New-Object System.Windows.Forms.Label
$lblNomeVM.Location = New-Object System.Drawing.Size(25,60) 
$lblNomeVM.Size = New-Object System.Drawing.Size(280,20) 
$lblNomeVM.Text = "Nome da Maquina Virtual:"
$Form1.Controls.Add($lblNomeVM)

$txtNomeVM = New-Object System.Windows.Forms.TextBox
$txtNomeVM.Location = New-Object System.Drawing.Size(25,80)
$txtNomeVM.Size = New-Object System.Drawing.Size(280,1)
$Form1.Controls.Add($txtNomeVM)

$lblResultado = New-Object System.Windows.Forms.Label
$lblResultado.Location = New-Object System.Drawing.Point(25, 225)
$lblResultado.Size = New-Object System.Drawing.Size(280, 1)
$lblResultado.Text = ""
$Form1.Controls.Add($lblResultado)


######### Texto escolha de Templates #########
##############################################
$label0 = New-Object System.Windows.Forms.Label
$label0.Location = New-Object System.Drawing.Size(25,20) 
$label0.Size = New-Object System.Drawing.Size(350,20) 
$label0.Text = 'Criação de máquinas virtuais baseado em Templates:'
$Form1.Controls.Add($label0)
##############################################

#### listagem dos Sistemas Operacionais ######
##############################################
$lblListaSO = New-Object System.Windows.Forms.Label
$lblListaSO.Location = New-Object System.Drawing.Size(25,110) 
$lblListaSO.Size = New-Object System.Drawing.Size(280,20) 
$lblListaSO.Text = "Selecione o Sistema Operacional:"
$Form1.Controls.Add($lblListaSO)

$caminhovm = Get-Content "C:\script-hyperv\caminho-vms.txt"
$caminhomodelo = Get-Content "C:\script-hyperv\caminho-modelo.txt"
$txtlistaso = Get-ChildItem -Path $caminhomodelo

$comboBox2 = New-Object System.Windows.Forms.ComboBox
$comboBox2.Location = New-Object System.Drawing.Point(25, 130)
$comboBox2.Size = New-Object System.Drawing.Size(350, 310)

foreach ($txtlistaso in $txtlistaso) 
{
      if ($txtlistaso.Attributes -ne "Directory")
      {
             $comboBox2.Items.add($txtlistaso)
      }
}
$Form1.Controls.Add($comboBox2)
##############################################

###### Seleção de Switch / Rede Virtual  #####
##############################################

$lblVmSwitch = New-Object System.Windows.Forms.Label
$lblVmSwitch.Location = New-Object System.Drawing.Size(25,160) 
$lblVmSwitch.Size = New-Object System.Drawing.Size(280,20) 
$lblVmSwitch.Text = "Selecione o Switch Virtual:"
$Form1.Controls.Add($lblVmSwitch)

$VMSwitches = Get-VMSwitch
$comboBox1 = New-Object System.Windows.Forms.ComboBox
$comboBox1.Location = New-Object System.Drawing.Point(25, 180)
$comboBox1.Size = New-Object System.Drawing.Size(350, 310)
foreach($VMSwitch in $VMSwitches)
{
  $comboBox1.Items.add($VMSwitch.Name)
}
$Form1.Controls.Add($comboBox1)
##############################################

############# Selecionar Template ############
##############################################
#$lblVmSwitch1 = New-Object System.Windows.Forms.Label
#$lblVmSwitch1.Location = New-Object System.Drawing.Size(25,210) 
#$lblVmSwitch1.Size = New-Object System.Drawing.Size(280,20) 
#$lblVmSwitch1.Text = "Selecione Template"
#$Form1.Controls.Add($lblVmSwitch1)

#$VMSwitches1 = ls C:\script-hyperv/templates
#$comboBox2 = New-Object System.Windows.Forms.ComboBox
#$comboBox2.Location = New-Object System.Drawing.Point(25, 230)
#$comboBox2.Size = New-Object System.Drawing.Size(350, 310)
#foreach($VMSwitch1 in $VMSwitches1)
#{
#  $comboBox2.Items.add($VMSwitch1.Name)
#}
#$Form1.Controls.Add($comboBox2)
##############################################

############# Selecionar Template ############
$infotemplate = New-Object System.Windows.Forms.Label
$infotemplate.Location = New-Object System.Drawing.Size(25,280) 
$infotemplate.Size = New-Object System.Drawing.Size(350,20) 
$infotemplate.Text = 'Escolha o template de acordo com a necessidade:'
$Form1.Controls.Add($infotemplate)

$template1 = New-Object System.Windows.Forms.Label
$template1.Location = New-Object System.Drawing.Size(25,300) 
$template1.Size = New-Object System.Drawing.Size(350,20) 
$template1.Text = 'T1 = 2 vCPU, 4G RAM, 50G SSD'
$Form1.Controls.Add($template1)

$template2 = New-Object System.Windows.Forms.Label
$template2.Location = New-Object System.Drawing.Size(25,320) 
$template2.Size = New-Object System.Drawing.Size(350,20) 
$template2.Text = 'T2 = 4 vCPU, 8G RAM, 100G SSD'
$Form1.Controls.Add($template2)

$template3 = New-Object System.Windows.Forms.Label
$template3.Location = New-Object System.Drawing.Size(25,340) 
$template3.Size = New-Object System.Drawing.Size(350,20) 
$template3.Text = 'T3 = 8 vCPU, 16G RAM, 200G SSD'
$Form1.Controls.Add($template3)

$template4 = New-Object System.Windows.Forms.Label
$template4.Location = New-Object System.Drawing.Size(25,360) 
$template4.Size = New-Object System.Drawing.Size(350,20) 
$template4.Text = 'T3 = 16 vCPU, 32G RAM, 400G SSD'
$Form1.Controls.Add($template4)
##############################################

#################### Botão que cria a VM T1 ###################
############################################################
$Button = New-Object System.Windows.Forms.Button
$Button.Location = New-Object System.Drawing.Point(25, 400)
$Button.Size = New-Object System.Drawing.Size(85, 23)
$Button.Text = "Criar VM T1"
$Button.add_Click({
                    $lblResultado.Text="Máquina Virtual " + $txtNomeVM.Text + " criada com sucesso."
                    $nomedoparent = $comboBox2.Text
                    $nomedavm = $txtNomeVM.Text
                    New-VHD -ParentPath "$caminhomodelo\$nomedoparent" -Path "$caminhovm\$nomedavm\$nomedavm.vhdx"
                    New-VM -Path $caminhovm -VHDPath "$caminhovm\$nomedavm\$nomedavm.vhdx" -Generation 1 -MemoryStartupBytes 4Gb -Name $nomedavm -SwitchName $comboBox1.Text | Set-VMMemory -DynamicMemoryEnabled $false
                    Get-VM $nomedavm | Set-VMProcessor -count 2
                    })
$Form1.Controls.Add($Button)


$lblResultado = New-Object System.Windows.Forms.Label
$lblResultado.Location = New-Object System.Drawing.Point(25, 225)
$lblResultado.Size = New-Object System.Drawing.Size(450, 150)
$lblResultado.Text = ""
$Form1.Controls.Add($lblResultado)
############################################################

############### Botão que cria a VM T2 ##############
############################################################
$Button = New-Object System.Windows.Forms.Button
$Button.Location = New-Object System.Drawing.Point(115, 400)
$Button.Size = New-Object System.Drawing.Size(85, 23)
$Button.Text = "Criar VM T2"
$Button.add_Click({
                    $lblResultado.Text="Máquina Virtual " + $txtNomeVM.Text + " criada com sucesso."
                    $nomedoparent = $comboBox2.Text
                    $nomedavm = $txtNomeVM.Text
                    New-VHD -ParentPath "$caminhomodelo\$nomedoparent" -Path "$caminhovm\$nomedavm\$nomedavm.vhdx"
                    New-VM -Path $caminhovm -VHDPath "$caminhovm\$nomedavm\$nomedavm.vhdx" -Generation 1 -MemoryStartupBytes 8Gb -Name $nomedavm -SwitchName $comboBox1.Text | Set-VMMemory -DynamicMemoryEnabled $false
                    Get-VM $nomedavm | Set-VMProcessor -count 4
                    })
$Form1.Controls.Add($Button)

$lblResultado = New-Object System.Windows.Forms.Label
$lblResultado.Location = New-Object System.Drawing.Point(25, 225)
$lblResultado.Size = New-Object System.Drawing.Size(450, 150)
$lblResultado.Text = ""
$Form1.Controls.Add($lblResultado)
############################################################

############### Botão que cria a VM T3 ##############
############################################################
$Button = New-Object System.Windows.Forms.Button
$Button.Location = New-Object System.Drawing.Point(205, 400)
$Button.Size = New-Object System.Drawing.Size(85, 23)
$Button.Text = "Criar VM T3"
$Button.add_Click({
                    $lblResultado.Text="Máquina Virtual " + $txtNomeVM.Text + " criada com sucesso."
                    $nomedoparent = $comboBox2.Text
                    $nomedavm = $txtNomeVM.Text
                    New-VHD -ParentPath "$caminhomodelo\$nomedoparent" -Path "$caminhovm\$nomedavm\$nomedavm.vhdx"
                    New-VM -Path $caminhovm -VHDPath "$caminhovm\$nomedavm\$nomedavm.vhdx" -Generation 1 -MemoryStartupBytes 16Gb -Name $nomedavm -SwitchName $comboBox1.Text | Set-VMMemory -DynamicMemoryEnabled $false
                    Get-VM $nomedavm | Set-VMProcessor -count 8
                    })
$Form1.Controls.Add($Button)

$lblResultado = New-Object System.Windows.Forms.Label
$lblResultado.Location = New-Object System.Drawing.Point(25, 225)
$lblResultado.Size = New-Object System.Drawing.Size(450, 150)
$lblResultado.Text = ""
$Form1.Controls.Add($lblResultado)
############################################################

############### Botão que cria a VM T4 ##############
############################################################
$Button = New-Object System.Windows.Forms.Button
$Button.Location = New-Object System.Drawing.Point(295, 400)
$Button.Size = New-Object System.Drawing.Size(85, 23)
$Button.Text = "Criar VM T4"
$Button.add_Click({
                    $lblResultado.Text="Máquina Virtual " + $txtNomeVM.Text + " criada com sucesso."
                    $nomedoparent = $comboBox2.Text
                    $nomedavm = $txtNomeVM.Text
                    New-VHD -ParentPath "$caminhomodelo\$nomedoparent" -Path "$caminhovm\$nomedavm\$nomedavm.vhdx"
                    New-VM -Path $caminhovm -VHDPath "$caminhovm\$nomedavm\$nomedavm.vhdx" -Generation 1 -MemoryStartupBytes 32Gb -Name $nomedavm -SwitchName $comboBox1.Text | Set-VMMemory -DynamicMemoryEnabled $false
                    Get-VM $nomedavm | Set-VMProcessor -count 16
                    })
$Form1.Controls.Add($Button)


$lblResultado = New-Object System.Windows.Forms.Label
$lblResultado.Location = New-Object System.Drawing.Point(25, 225)
$lblResultado.Size = New-Object System.Drawing.Size(450, 150)
$lblResultado.Text = ""
$Form1.Controls.Add($lblResultado)
############################################################


[void]$form1.showdialog()

##############################################
#### Script para Linux
# hostname
# IP
# Netbackup Client
# Monitoramento Nagios\Zabbix
# Wazuh
# SNMP

#Fontes:
# https://cooperati.com.br/2018/08/script-vm-powershell/
# https://www.youtube.com/watch?v=XAV9DxSBmmE&t=583s&ab_channel=GabrielLuiz
